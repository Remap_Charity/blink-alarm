'''
Tkinter Application for a Remap blink-alarm.
Rupert Powell
Created in 12-2019

'''
# Tk Video test
# import the necessary packages
from __future__ import print_function
from imutils.video import VideoStream
from imutils import face_utils
from scipy.spatial import distance as dist
import argparse
import time
from PIL import Image
from PIL import ImageTk
import tkinter as tki
import threading
import datetime
import imutils
import dlib
import cv2
import os
import numpy as np
from relay_lib_seeed import *


def eye_aspect_ratio(eye):
	# compute the euclidean distances between the two sets of
	# vertical eye landmarks (x, y)-coordinates
	A = dist.euclidean(eye[1], eye[5])
	B = dist.euclidean(eye[2], eye[4])

	# compute the euclidean distance between the horizontal
	# eye landmark (x, y)-coordinates
	C = dist.euclidean(eye[0], eye[3])

	# compute the eye aspect ratio
	ear = (A + B) / (2.0 * C)

	# return the eye aspect ratio
	return ear

class BlinkDetectApp:
	def __init__(self, vs):
		# store the video stream object and output path, then initialize
		# the most recently read frame, thread for reading frames, and
		# the thread stop event
		self.vs = vs
		#self.outputPath = outputPath
		self.frame = None
		self.thread = None
		self.stopEvent = None

		# initialize the root window and image panel
		self.root = tki.Tk()
		self.panel = None

		
		
		self.topframe = tki.Frame(height=100)
		self.topframe.pack_propagate(0)
		self.topframe.pack(fill=tki.BOTH)

		# self.bottomframe = tki.Frame(height=10)
		# self.bottomframe.pack_propagate(0)
		# self.bottomframe.pack(fill=tki.BOTH)

		self.Blink1 = tki.Button(self.topframe, text='1', padx=40, pady=40)
		self.Blink2 = tki.Button(self.topframe, text='2', padx=40, pady=40)
		self.Blink3 = tki.Button(self.topframe, text='3', padx=40, pady=40)
		
		self.blinks = [self.Blink1, self.Blink2, self.Blink3]
		
		#self.Alarm = tki.Button(self.bottomframe, text='ALARM', padx=36, pady=40, command=self.alarm_cancel)
		# TestAlarm = tki.Button(bottomframe, text='Test-alarm', padx=36, pady=40, command=self.alarm_on)
		#self.FaceDetect = tki.Button(self.bottomframe, text='FACE', padx=40, pady=40)
		self.Blink1.pack(fill=tki.BOTH, expand=1, padx=10, pady=10, side=tki.LEFT)
		self.Blink2.pack(fill=tki.BOTH, expand=1, padx=10, pady=10, side=tki.LEFT)
		self.Blink3.pack(fill=tki.BOTH, expand=1, padx=10, pady=10, side=tki.LEFT)
		# Alarm.pack(fill=tki.BOTH, expand=1, padx=10, pady=10, side=tki.LEFT)
		# Alarm.place(width=150, height=100, relx = 0.011, rely = 0.74)
		# TestAlarm.pack(side="left", padx=10, pady=10)
		# FaceDetect.pack(fill=tki.BOTH, expand=1, padx=10, pady=10, side=tki.LEFT)
		# FaceDetect.place(width=150, height=100, relx = 0.813, rely = 0.74)

		# start a thread that constantly pools the video sensor for
		# the most recently read frame
		self.stopEvent = threading.Event()
		self.thread = threading.Thread(target=self.videoLoop, args=())
		self.thread.start()

		# set a callback to handle when the window is closed
		self.root.wm_title("Remap Blink Alarm")
		self.root.wm_protocol("WM_DELETE_WINDOW", self.onClose)
		
		# # construct the argument parse and parse the arguments
		# ap = argparse.ArgumentParser()
		# ap.add_argument("-p", "--shape-predictor", required=True,
		# 	help="path to facial landmark predictor")
		# ap.add_argument("-v", "--video", type=str, default="",
		# 	help="path to input video file")
		# args = vars(ap.parse_args())
		
		# define two constants, one for the eye aspect ratio to indicate
		# blink and then a second constant for the number of consecutive
		# frames the eye must be below the threshold
		self.EYE_AR_THRESH = 0.2
		self.EYE_AR_CONSEC_FRAMES = 2

		# initialize the frame counters and the total number of blinks
		self.COUNTER = 0
		self.TOTAL = 0
		self.check_time = 0

		# initialize dlib's face detector (HOG-based) and then create
		# the facial landmark predictor
		print("[INFO] loading facial landmark predictor...")
		self.detector = dlib.get_frontal_face_detector()
		self.predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

		# grab the indexes of the facial landmarks for the left and
		# right eye, respectively
		(self.lStart, self.lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
		(self.rStart, self.rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

		# start the video stream thread
		print("[INFO] starting video stream thread...")
		# vs = FileVideoStream(args["video"]).start()
		# fileStream = True
		#vs = VideoStream(src=0).start()
		#vs = VideoStream(usePiCamera=True).start()
		#time.sleep(2.0)

	def clear_blinks(self):
		for blink in self.blinks:
			blink.config(bg='gray90')

	def videoLoop(self):
		# DISCLAIMER:
		# I'm not a GUI developer, nor do I even pretend to be. This
		# try/except statement is a pretty ugly hack to get around
		# a RunTime error that Tkinter throws due to threading
		try:
			# keep looping over frames until we are instructed to stop
			while not self.stopEvent.is_set():
				#print('X')
				# grab the frame from the video stream and resize it to
				# have a maximum width of 300 pixels
				self.frame = self.vs.read()
				self.frame = imutils.resize(self.frame, width=450)
				gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
				# detect faces in the grayscale frame
				rects = self.detector(gray, 0)
				for rect in rects:
					# determine the facial landmarks for the face region, then
					# convert the facial landmark (x, y)-coordinates to a NumPy
					# array
					shape = self.predictor(gray, rect)
					shape = face_utils.shape_to_np(shape)
					# extract the left and right eye coordinates, then use the
					# coordinates to compute the eye aspect ratio for both eyes
					leftEye = shape[self.lStart:self.lEnd]
					rightEye = shape[self.rStart:self.rEnd]
					leftEAR = eye_aspect_ratio(leftEye)
					rightEAR = eye_aspect_ratio(rightEye)
					# average the eye aspect ratio together for both eyes
					ear = (leftEAR + rightEAR) / 2.0
					# compute the convex hull for the left and right eye, then
					# visualize each of the eyes
					leftEyeHull = cv2.convexHull(leftEye)
					rightEyeHull = cv2.convexHull(rightEye)
					cv2.drawContours(self.frame, [leftEyeHull], -1, (0, 255, 0), 1)
					cv2.drawContours(self.frame, [rightEyeHull], -1, (0, 255, 0), 1)
					# check to see if the eye aspect ratio is below the blink
					# threshold, and if so, increment the blink frame counter
					if ear < self.EYE_AR_THRESH:
						self.COUNTER += 1
					# otherwise, the eye aspect ratio is not below the blink
					# threshold
					else:
						# if the eyes were closed for a sufficient number of
						# then increment the total number of blinks
						if self.COUNTER >= self.EYE_AR_CONSEC_FRAMES:
							self.TOTAL += 1
							# print('Blinks {}'.format(TOTAL))
							self.blinks[self.TOTAL-1].config(bg='red')
							self.check_time = 0
						# reset the eye frame counter
						self.COUNTER = 0
					# draw the total number of blinks on the frame along with
					# the computed eye aspect ratio for the frame
					# cv2.putText(frame, "Blinks: {} {}".format(TOTAL, check_time), (10, 30),
					# 	cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
					cv2.putText(self.frame, "EAR: {:.2f}".format(ear), (300, 30),
						cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
					# OpenCV represents images in BGR order; however PIL
					# represents images in RGB order, so we need to swap
					# the channels, then convert to PIL and ImageTk format
				image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
				image = Image.fromarray(image)
				image = ImageTk.PhotoImage(image)
				# if the panel is not None, we need to initialize it
				if self.panel is None:
					print('Create Video panel')
					self.panel = tki.Label(image=image)
					self.panel.image = image
					self.panel.pack(side="bottom", padx=10, pady=10)
				# otherwise, simply update the panel
				else:
					self.panel.configure(image=image)
					self.panel.image = image
				# check to see if enough blinks have been seen to raise the alarm
				self.check_time +=1
				if self.check_time > 20:
					#print('Timeout')
					self.check_time = 0
					if self.TOTAL > 0:
						self.TOTAL -= 1
					self.blinks[self.TOTAL].config(bg='grey90')
					relay_off(1)
				if self.TOTAL > 2:
					print('ALARM')
					self.TOTAL = 0
					self.check_time = 0
					self.root.after(2000, self.clear_blinks)
					relay_on(1)

		except RuntimeError as e:
			print("[INFO] caught a RuntimeError", e)

	def onClose(self):
		# set the stop event, cleanup the camera, and allow the rest of
		# the quit process to continue
		print("[INFO] closing...")
		relay_off(1)
		self.stopEvent.set()
		self.vs.stop()
		self.root.quit()
	
# initialize the video stream and allow the camera sensor to warmup
print("[INFO] warming up camera...")
vs = VideoStream(usePiCamera=True).start()
time.sleep(2.0)

# start the app
pba = BlinkDetectApp(vs)
pba.root.mainloop()