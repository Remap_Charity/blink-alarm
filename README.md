Installing Blink Detection on Raspberry Pi 3+
4-2-19 Rupert Powell

To Do
------------------------------------------
Make it as one tkinter app

Wire PSU / Fuse up DONE

Wire in relay DONE

Screw in camera DONE

Test from bed position

Set-up dark theme

Trim yoke bolts

Fit wingnuts

Provide instructions and Schematics

Contact details on TK app

Create a log file to track usage

Create a JSON settings file

-------------------------------------------


Install Buster from RasperryPi website.

User:pi password:hello

Enable SSH, VNC, Camera and I2C

Once SSH is enabled logon from PC.

>mkdir blink

Followed this to install opencv
https://stackoverflow.com/questions/59080094/raspberry-pi-and-opencv-cant-install-libhdf5-100


Then install imutils from piimage-research
>sudo pip install imutils

Then follow this guide to imstalling dlib
https://www.pyimagesearch.com/2018/01/22/install-dlib-easy-complete-guide/

To detect the I2C buss devices (relay board)
>i2cdetect -y -r 1

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- -- 

Relay board shows up as device 20

To launch app at startup:
create an .sh file:
#!/bin/sh
cd ~/blink
python3 blink.py --shape-predictor shape_predictor_68_face_landmarks.dat

Then:
>cd /etc/xdg/lxsession/LXDE-pi/
>sudo nano autostart

Add this to the end....

@sh /home/pi/Desktop/blink.sh

Disable the screen saver and blanking
>sudo nano /etc/xdg/lxsession/LXDE-pi/autostart

add the following lines
@xset s noblank 
@xset s off 
@xset -dpms

To monitor the Pi temperature
>/opt/vc/bin/vcgencmd measure_temp
80 is max reccomended
60 is normal

