# USAGE
# This is a basic version just with 1 window as shipped on 7-12-19
# type "Q" to quit 
# python3 simpleblink.py

# import the necessary packages
from scipy.spatial import distance as dist
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils import face_utils
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2
from tkinter import *
from relay_lib_seeed import *
import datetime

root = Tk()

def alarm_cancel():
	print('ALARM Cancelled')
	# Alarm.config(bg='gray90')
	relay_off(1)

def alarm_on():
	print('Alarm ON by button')
	relay_on(1)

# Blink1 = Button(root, text='1', padx=40, pady=40)
# Blink2 = Button(root, text='2', padx=40, pady=40)
# Blink3 = Button(root, text='3', padx=40, pady=40)
# #Blink4 = Button(root, text='4', padx=40, pady=40)
# #Blink5 = Button(root, text='5', padx=40, pady=40)
# Alarm = Button(root, text='ALARM', padx=36, pady=40, command=alarm_cancel)
# FaceDetect = Button(root, text='FACE', padx=40, pady=40)
# Blink1.grid(row=0,column=0)
# Blink2.grid(row=0,column=1)
# Blink3.grid(row=0,column=2)
# #Blink4.grid(row=0,column=3)
# #Blink5.grid(row=0,column=4)
# Alarm.grid(row=1,column=0)
# FaceDetect.grid(row=1,column=2)

# blinks = [Blink1, Blink2, Blink3] #Blink4, Blink5]

def clear_blinks():
	# Alarm.config(bg='gray90')
	for blink in blinks:
		blink.config(bg='gray90')
	pass



def eye_aspect_ratio(eye):
	# compute the euclidean distances between the two sets of
	# vertical eye landmarks (x, y)-coordinates
	A = dist.euclidean(eye[1], eye[5])
	B = dist.euclidean(eye[2], eye[4])

	# compute the euclidean distance between the horizontal
	# eye landmark (x, y)-coordinates
	C = dist.euclidean(eye[0], eye[3])

	# compute the eye aspect ratio
	ear = (A + B) / (2.0 * C)

	# return the eye aspect ratio
	return ear
 
# construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-p", "--shape-predictor", required=True,
# 	help="path to facial landmark predictor")
# ap.add_argument("-v", "--video", type=str, default="",
# 	help="path to input video file")
# args = vars(ap.parse_args())
 
# define two constants, one for the eye aspect ratio to indicate
# blink and then a second constant for the number of consecutive
# frames the eye must be below the threshold
EYE_AR_THRESH = 0.19
EYE_AR_CONSEC_FRAMES = 2

# initialize the frame counters and the total number of blinks
COUNTER = 0
TOTAL = 0
check_time = 0

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
print("[INFO] loading facial landmark predictor...")
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# grab the indexes of the facial landmarks for the left and
# right eye, respectively
(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

# start the video stream thread
print("[INFO] starting video stream thread...")
# vs = FileVideoStream(args["video"]).start()
# fileStream = True
#vs = VideoStream(src=0).start()
vs = VideoStream(usePiCamera=True).start()
fileStream = False
time.sleep(2.0)

# loop over frames from the video stream
# Make the GUI visable
	
while True:
	
	# if this is a file video stream, then we need to check if
	# there any more frames left in the buffer to process
	if fileStream and not vs.more():
		break

	# grab the frame from the threaded video file stream, resize
	# it, and convert it to grayscale
	# channels)
	frame = vs.read()
	frame = imutils.resize(frame, width=450)
	frame = cv2.flip(frame, 1)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	# detect faces in the grayscale frame
	rects = detector(gray, 0)

	# if rects:
	# 	FaceDetect.config(bg='green')
	# else:
	# 	FaceDetect.config(bg='gray90')

	# loop over the face detections
	for rect in rects:
		# determine the facial landmarks for the face region, then
		# convert the facial landmark (x, y)-coordinates to a NumPy
		# array
		shape = predictor(gray, rect)
		shape = face_utils.shape_to_np(shape)

		# extract the left and right eye coordinates, then use the
		# coordinates to compute the eye aspect ratio for both eyes
		leftEye = shape[lStart:lEnd]
		rightEye = shape[rStart:rEnd]
		leftEAR = eye_aspect_ratio(leftEye)
		rightEAR = eye_aspect_ratio(rightEye)

		# average the eye aspect ratio together for both eyes
		ear = (leftEAR + rightEAR) / 2.0
		# ear = leftEAR

		# compute the convex hull for the left and right eye, then
		# visualize each of the eyes
		leftEyeHull = cv2.convexHull(leftEye)
		rightEyeHull = cv2.convexHull(rightEye)
		cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
		cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)

		# check to see if the eye aspect ratio is below the blink
		# threshold, and if so, increment the blink frame counter
		if ear < EYE_AR_THRESH:
			COUNTER += 1
			

		# otherwise, the eye aspect ratio is not below the blink
		# threshold
		else:
			# if the eyes were closed for a sufficient number of
			# then increment the total number of blinks
			if COUNTER >= EYE_AR_CONSEC_FRAMES:
				TOTAL += 1
				# print('Blinks {}'.format(TOTAL))
				# blinks[TOTAL-1].config(bg='red')
				check_time = 0
				# root.update()
			# reset the eye frame counter
			COUNTER = 0

		

		# draw the total number of blinks on the frame along with
		# the computed eye aspect ratio for the frame
		cv2.putText(frame, "{}".format(TOTAL), (10, 100),
			cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 255), 2)
		cv2.putText(frame, "EAR: {:.2f}".format(ear), (300, 30),
			cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
	
	# check to see if enough blinks have been seen to raise the alarm
	check_time +=1
	if check_time > 20:
		#print('Timeout')
		check_time = 0
		if TOTAL > 0:
			TOTAL -= 1
		# blinks[TOTAL].config(bg='grey90')
		relay_off(1)
		# root.update()
	if TOTAL > 2:
		print('[EVENT] ALARM - {}'.format(datetime.datetime.now()))
		
		TOTAL = 0
		check_time = 0
		# Alarm.config(bg='green')
		# root.after(2000, clear_blinks)
		relay_on(1)
		# root.update()

	# show the frame
	cv2.imshow("Remap Blink Alarm", frame)
	key = cv2.waitKey(1) & 0xFF
 
	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break

# do a bit of cleanup
relay_off(1)
cv2.destroyAllWindows()
vs.stop()
